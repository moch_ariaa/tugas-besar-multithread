import time
import threading

#Fungsi menentukan bilang prima
def prima(data):
    bilprim = []
    notprim = []
    for num in data:
        time.sleep(0.1)
        if num > 1:
            for i in range(2, num):
                if (num % i) == 0:
                    notprim.append(num)
                    print("bukan bilangan prima: ", notprim)
                    break
            else:
                bilprim.append(num)
                # print("bilangan prima: ", bilprim)


#Kita akan memasukkan batas bawah dan atas dalam sebuah pencarian
lower = int(input("Masukkan batas bawah: "))
upper = int(input("Masukkan batas atas: "))

#kita akan membagi datanya menjadi 2
mid = (lower + upper) // 2

#ktia siapkan array penampung untuk menampung data yang dibagi 2 tadi
data=[]
dataa=[]

#memasukkan data dari awal ke tengah data (yang dibagi dua tadi)
for i in range(lower, mid+1):
        data.append(i)

#memasukkan data dari tengah ke akhir data (yang dibagi dua tadi)
for i in range(mid+1,upper+1):
        dataa.append(i)

#memanggil .time
t = time.time()

#membuat thread untuk mengerjakan fungsi tadi dengan 2 array yg dibagi sebelumnya
t1= threading.Thread(target=prima, args=(data,))
t2= threading.Thread(target=prima, args=(dataa,))

#thread 1 dan 2 dijalankan
t1.start()
t2.start()

#thread 1 dan 2 dijoinkan
t1.join()
t2.join()

print("")
print("waktu total : ", time.time()-t)