import time
import threading

#variabel global untuk menyimpan nilai jumlah bilangan prima
jumPrim = 0

#fungsi untuk menjumlahkan bilangan prima dimana akan dicari
#terlebih dahulu yang menjadi bilangan prima lalu dijumlahkan
def jumPrima(data):
    global jumPrim
    for num in data:
        time.sleep(0.1)
        if num > 1:
            for i in range(2,num):
                if (num % i) == 0:
                    break
            else:
                print("Ketemu bilangan Prima yaitu : ", end='')
                print(num)
                jumPrim += num
                print("Jumlah bilangan Prima sekarang : ", end='')
                print(jumPrim)

#akan diminta input untuk menentukan batas bawah dan batas atas
#sebagai batas pencarian datanya
lower = int(input("Masukkan batas bawah: "))
upper = int(input("Masukkan batas atas: "))

#data akan dibagi menjadi 2 karena kita akan menggunakan 2 thread sehingga
#datanya akan kita bagi menjadi 2 agar dikerjakan tiap threadnya masing - masing
mid = (lower + upper) // 2

#kita akan menyiapkan array penampung untuk menyimpan data
data=[]
dataa=[]

#data tadi akan dimasukkan kedalam array dengan pengulangan
for i in range(lower, mid+1):
        data.append(i)

for i in range(mid+1,upper+1):
        dataa.append(i)

#masing2 data array tadi akan dijalankan fungsinya kedalam 2 thread
#yaitu t1 dan t2 sehingga dijalankan secara bersamaan

#fungsi penghitung waktu python
t = time.time()

#proses inisialisasi thread
t1= threading.Thread(target=jumPrima, args=(data,))
t2= threading.Thread(target=jumPrima, args=(dataa,))

#thread dimulai
t1.start()
t2.start()

#thread dijalankan bersamaan
t1.join()
t2.join()

print("")
print("waktu total : ", time.time()-t)