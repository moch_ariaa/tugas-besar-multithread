import time
import threading

#variabel global untuk menyimpan nilai jumlah bilangan prima
jumPrim = 0

#fungsi untuk menjumlahkan bilangan prima dimana akan dicari
#terlebih dahulu yang menjadi bilangan prima lalu dijumlahkan
def jumPrima(data):
    global jumPrim
    for num in data:
        time.sleep(0.1)
        if num > 1:
            for i in range(2,num):
                if (num % i) == 0:
                    break
            else:
                print("Ketemu bilangan Prima yaitu : ", end='')
                print(num)
                jumPrim += num
                print("Jumlah bilangan Prima sekarang : ", end='')
                print(jumPrim)
                
#akan diminta input untuk menentukan batas bawah dan batas atas
#sebagai batas pencarian datanya
lower = int(input("Masukkan batas bawah: "))
upper = int(input("Masukkan batas atas: "))

#kita akan menyiapkan array penampung untuk menyimpan data
data=[]

#data tadi akan dimasukkan kedalam array dengan pengulangan
for i in range(lower, upper+1):
        data.append(i)

#fungsi penghitung waktu python
t = time.time()

#akan dijalankan fungsi prima dengan inputan array data
jumPrima(data)

print("")
print("waktu total : ", time.time()-t)