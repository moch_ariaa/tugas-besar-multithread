import time
import threading

def prima(data):
        for num in data:
                time.sleep(0.1)
                if num > 1:
                        for i in range(2,num):
                                if (num % i) == 0:
                                        break
                        else:
                                print("Thread",threading.current_thread(),"|",num, "pangkat 2 = ", pow(num, 2), sep = ' ')

lower = int(input("Masukkan batas bawah: "))
upper = int(input("Masukkan batas atas: "))
mid = (lower + upper) // 2
# print(mid)

data=[]
dataa=[]

for i in range(lower, mid+1):
        data.append(i)

for i in range(mid+1,upper+1):
        dataa.append(i)

t = time.time()

t1 = threading.Thread(target = prima, args = (data, ))
t2 = threading.Thread(target = prima, args = (dataa, ))

# prima(data)
# prima(dataa)

t1.start()
t2.start()

t1.join()
t2.join()

print("")
print("waktu total : ", time.time()-t)