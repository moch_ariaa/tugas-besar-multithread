import time
import threading

#Fungsi menentukan bilang prima
def prima(data):
    for num in data:
        time.sleep(0.1)
        if num > 1:
            for i in range(2,num):
                if (num % i) == 0:
                    break
            else:
                print(num)

#Kita akan memasukkan batas bawah dan atas dalam sebuah pencarian

batas = int(input("Masukkan batas angka yang akan dicari: "))

#kita akan membagi datanya menjadi 2
mid = batas // 2 #data tengah
mid_low = mid // 2 #data tengah pada bagian bawah (data_1 - data_2)
mid_up = (mid+batas) // 2 #data tengah pada bagian atas (data_3 - data_4)

#ktia siapkan array penampung untuk menampung data yang dibagi 2 tadi
data_1=[]
data_2=[]
data_3=[]
data_4=[]

#memasukkan data dari awal ke tengah data bawah untuk data_1
for i in range(1,mid_low):
        data_1.append(i)

#memasukkan data dari awal ke tengah data bawah untuk data_2
for i in range(mid_low,mid):
        data_2.append(i)

#memasukkan data dari awal ke tengah data bawah untuk data_3
for i in range(mid,mid_up):
        data_3.append(i)

#memasukkan data dari awal ke tengah data bawah untuk data_4
for i in range(mid_up,batas+1):
        data_4.append(i)

#memanggil .time
t = time.time()

#membuat thread untuk mengerjakan fungsi tadi dengan 2 array yg dibagi sebelumnya
t1= threading.Thread(target=prima, args=(data_1,))
t2= threading.Thread(target=prima, args=(data_2,))
t3= threading.Thread(target=prima, args=(data_3,))
t4= threading.Thread(target=prima, args=(data_4,))

#thread 1 dan 2 dijalankan
t1.start()
t2.start()
t3.start()
t4.start()

#thread 1 dan 2 dijoinkan
t1.join()
t2.join()
t3.join()
t4.join()

print("")
print("waktu total : ", time.time()-t)
